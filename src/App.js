import { useEffect, useState } from 'react';
import './App.css';


function App() {
  const [name, setName] = useState('');
  const [number, setNumber] = useState('');
  const [checkin, setCheckin] = useState('');
  const [checkout, setCheckout] = useState('');
  const [showForm, setShowForm] = useState(false);
  const [vehicles, setVehicles] = useState([]);
  const [emptyInput, setEmptyInput] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [correctInput, setCorrectInput] = useState(false);

  useEffect(() => {
    setCheckin(new Date().toLocaleString())
  }, [checkin])

  const handleCancel = () => {
    setShowForm(false);
    setErrorMessage(false);
    setEmptyInput(false);
    setName('');
    setNumber('');
    setCheckin('');
    setCheckout('');
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    if(!name || !number || !checkin || !checkout) {
      return (
        setEmptyInput(true),
        setErrorMessage(true)
      )
    }

    const newVehicle = {
      name, 
      number, 
      checkin, 
      checkout: new Date(checkout).toLocaleString()
    }

    setVehicles([newVehicle, ...vehicles])

    handleCancel();
  }

  return (
    <div className="app">
      <h1>Parking App</h1>

      <h3 style={{textAlign: 'center'}}>Total number of cars in garage: {vehicles.length}</h3>
      {vehicles.length <= 0 && <h3>Begin by adding cars to the garage</h3>}

      <div className='container'>
      {vehicles.length > 0 && 
        <div className='content'>
          <table cellSpacing={0}>
            <thead>
              <tr>
              <th>Driver Name</th>
              <th>Vehicle Number</th>
              <th>Checkin Time</th>
              <th>Checkout Time</th>
              </tr>
            </thead>
            <tbody>
              {vehicles.map((vehicle, index) => {
                return (
                  <tr key={index}>
                    <td>{vehicle.name}</td>
                    <td>{vehicle.number}</td>
                    <td>{vehicle.checkin}</td>
                    <td>{vehicle.checkout}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      }
        <div className='create'>
          <p onClick={() => setShowForm(!showForm)}>+</p>
        </div>

      </div>

      {showForm && (
      <div className='form'>
        <form onSubmit={handleSubmit}>
          <div className='formGroup'>
            <label>Driver Name</label>
            <input 
              type="text"
              value={name}
              onChange={(e) => {
                setName(e.target.value)
              }}
              className={`${!name && emptyInput && 'emptyInput'} ${name.length > 0 && correctInput && 'correctInput'}`}
            />
            {!name && errorMessage && <p className='errorMessage'>Driver name is required</p> }
          </div>

          <div className='formGroup'>
            <label>Vehicle Number</label>
            <input 
              type="text" 
              value={number}
              onChange={(e) => {
                setNumber(e.target.value)
                setCorrectInput(true)
              }}
              className={`${!number && emptyInput && 'emptyInput'} ${number.length > 0 && correctInput && 'correctInput'}`}
            />
            {!number && errorMessage && <p className='errorMessage'>Vehicle number is required</p> }

          </div>
          <div className='formGroup'>
            <label>Checkin Time</label>
            <input 
              type="text"
              value={checkin}
              onChange={(e) => {
                setCorrectInput(true)
              }}
              className={`${!checkin && emptyInput && 'emptyInput'} ${checkin.length > 0 && correctInput && 'correctInput'}`}
            />
            {!checkin && errorMessage && <p className='errorMessage'>Checkin time is required</p> }
          </div>

          <div className='formGroup'>
            <label>Checkout Time</label>
            <input 
              type="datetime-local" 
              value={checkout}  
              onChange={(e) => {
                setCheckout(e.target.value)
                setCorrectInput(true)
              }}
              className={`${!checkout && emptyInput && 'emptyInput'} ${checkout.length > 0 && correctInput && 'correctInput'}`}
            />
            {!checkout && errorMessage && <p className='errorMessage'>Checkout time is required</p> }
          </div>

          <div className='btns'>
            <button className='saveBtn'>Save</button>
            <button className='cancelBtn' onClick={handleCancel}>Cancel</button>
          </div>
        </form>
      </div>
      )}
    </div>
  );
}

export default App;
